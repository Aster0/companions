<div align="center">
![Image of companionsLogo](https://i.imgur.com/j6XwkxC.png)

![Image of overView](https://i.imgur.com/pLWrOFC.png )

Companions allows you to easily create your own custom companions, for your players to own. Upon activating a companion, it follows you beside your shoulder and gives you the companion's unique ability. 

*(Each companions can have any amount of abilities, read the WIKI here to learn how to create your own custom companions.)* 



<br>

---
<br>

![Image of features](https://i.imgur.com/7frb5Au.png)
*  95% of the plugin is customizable, meaning, community translations will be availble.
*  Easily add any amount of custom companions - make exclusive donor companions that doesn't appear in the shop but only allow your donors to own them.
*  Adjust body parts of a companion, to make it unique only to your server. - This allows you to make amazing looking companions.
*  A companion can have basically unlimited abilities, custom abilities, potion abilities.
*  Easy to navigate GUI, not needing your players to learn much before understanding how the plugin works.
*  Custom abilities for you to choose from and configure, allowing you to place on your companions. (SOME ABILITIES INCLUDE, FIREBALL, FLY... AND MORE!)
*  Allow your players to rename, change weapon, upgrade ability level and more, on their companions!​
*  Interactable Companions
*  **MySQL support** - Link your companions over multiple servers!





---

<br>

![Image of wiki](https://i.imgur.com/ZMODwDy.png)
<br>

If you have any doubts on any features of the plugin, visit our [wiki](https://gitlab.com/Aster0/companions/wikis/home). It boasts a really comprehensive wiki, allowing you to hopefully understand each and every features of the plugin.

---

<br>

Got an issue or a suggestion? Make a new post [here](https://gitlab.com/Aster0/companions/issues), I am regularly checking them and will reply to you in less than a day max. If you ever need support on how to use a certain feature of the plugin, join the Discord support server here.


<br>

![Image of dragon](https://i.gyazo.com/400fdd040ee91fb2f67b4691401e8a96.png) ![Image of turtle](https://i.gyazo.com/4ea22f4f8d212713c2a034914cef6a70.png)
